var RtmClient = require('@slack/client').RtmClient;
var CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;
var RTM_EVENTS = require('@slack/client').RTM_EVENTS;

var request = require('request');

var bot_token = '<slack_token>';
var rtm = new RtmClient(bot_token);

var weatherToken = '<openweather_api_key>';


var makeMention = function(userId) {
    return '<@' + userId + '>';
};
 
var isDirect = function(userId, messageText) {
    var userTag = makeMention(userId);
    return messageText &&
           messageText.length >= userTag.length &&
           messageText.substr(0, userTag.length) === userTag;
};

let channel;

// The client will emit an RTM.AUTHENTICATED event on successful connection, with the `rtm.start` payload
rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (rtmStartData) => {
  for (const c of rtmStartData.channels) {
	  if (c.is_member && c.name ==='my-bots') { channel = c.id }
  }
  console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}`);
});


// you need to wait for the client to fully connect before you can send messages

rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, function () {

    sd = rtm.dataStore;


    var channels = Object.keys(sd.channels)
        .map(function (k) { return sd.channels[k]; })
        .filter(function (c) { return c.is_member; })
        .map(function (c) { return c.name; });
 
    var groups = Object.keys(sd.groups)
        .map(function (k) { return sd.groups[k]; })
        .filter(function (g) { return g.is_open && !g.is_archived; })
        .map(function (g) { return g.name; });

        if (channels.length > 0) {
            console.log('You are in: ' + channels.join(', '));
        }
        else {
            console.log('You are not in any channels.');
        }
     
        if (groups.length > 0) {
           console.log('As well as: ' + groups.join(', '));
        }

});

rtm.on(RTM_EVENTS.MESSAGE, function(message) {
   
        var channel = message.channel;
        var user = message.user;

        console.log("User: " + user + " | Channel: " + channel);

    if (message.type === 'message' && isDirect(rtm.activeUserId, message.text)) {

         processMessage(message, rtm);

        function processMessage(message, rtm) {
            var locationName = message.text;
            var query = (isNaN(locationName) ? 'q=' + locationName : 'zip=' + locationName) + '&units=metric&APPID=' + weatherToken;
            rtm.sendMessage('I\'ll get you the current weather for "' + locationName + '"', channel, function() {
                getAndSendCurrentWeather(locationName, query, channel, rtm);
            });
        }

        function getAndSendCurrentWeather(locationName, query, channel, rtm) {
            request('http://api.openweathermap.org/data/2.5/weather?' + query, function(error, response, body) {
                var weatherData = JSON.parse(body);
                if (weatherData.cod == "404") {
                    rtm.sendMessage('I\'m sorry, but I couldn\'t find a city called "' + locationName + '"', channel, function() {
                        getAndSendForcast(locationName, channel, rtm);
                    });
                } else {
                    var message = 'The weather in ' + weatherData.name + ' is ' + weatherData.weather[0].main + ' and it\'s ' + weatherData.main.temp + ' degrees.';
                    rtm.sendMessage(message, channel, function() {
                        getAndSendForcast(locationName, query, channel, rtm);
                    });
                }
            });
        }

        function getAndSendForcast(locationName, query, channel, rtm) {
            request('http://api.openweathermap.org/data/2.5/forecast/daily?' + query, function(error, response, body) {
                var message;
                var weatherData = JSON.parse(body);
                if (weatherData.cod == "404") {
                    message = 'I\'m sorry, but I couldn\'t find a city called "' + locationName + '"';
                } else {
                    message = 'The high is ' + weatherData.list[0].temp.max + ' and the low is ' + weatherData.list[0].temp.min;
                }
                rtm.sendMessage(message, channel, function() {});
             
            });
        }

    }
 });  
        

rtm.start();




